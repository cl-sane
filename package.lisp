(defpackage
    sane-lowlevel
  (:use cl cffi)
  (:export :ensure-ok))

(defpackage
    sane
  (:use cl iterate cffi sane-lowlevel trivial-gray-streams)
  (:export
   ;; Library
   :with-init
   :sane-ref :sane-unref
   ;; Device
   :devices
   :name :vendor :model :device-type :device-option
   :with-device
   ;; Option getters
   :get-name :get-title :get-description :get-device
   :get-capabilities :get-constraint
   ;; Constraints
   :get-min :get-step :get-max :get-list
   ;; Main option functions
   :option-value-readable-p
   :option :option-value
   ;; And option debugging
   :dump-option-list
   ;; Reading
   :with-open-scanner-stream :parameters :device))

(in-package :sane)

(define-foreign-library libsane
    (:unix (:or "libsane.so.1" "libsane.so"))
    (t (:default "libsane")))
(use-foreign-library libsane)
