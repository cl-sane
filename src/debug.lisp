(in-package :sane)

(defmethod dump-option-list ((d device) &optional (stream *standard-output*))
  (iterate
    (for group in (device-groups d))
    (format stream "~A~%" group)))
