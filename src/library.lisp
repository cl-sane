(in-package :sane)

(defvar *sane-references* 0)
(defvar *sane-version* nil)

(defun sane-interpret-version (n)
  "Read bitstring for version and output the correct major/minor/rev
version"
  (list
   (ash n -24)                          ; major
   (boole boole-and (ash n -16) 255)    ; minor
   (boole boole-and n 65535)))

(defmacro with-init (version-sym &body body)
  "Run BODY with VERSION-SYM set to a triple representing the version of libsane
that is running. Evaluates to BODY. If VERSION-SYM is nil, there is no symbol
bound to the version."
  (let ((always-version (gensym)))
    `(unwind-protect
          (let* ((,always-version (sane-ref))
                 ,@(when version-sym `((,version-sym ,always-version))))
            ;; To ignore complaints about this being unused. But don't evaluate
            ;; to it.
            ,always-version nil

            ,@body)
       (sane-unref))))

(defun sane-ref ()
  "Initialise the SANE library and return a triple returning the current version
  of libsane. Add a reference to the library structures."
  (incf *sane-references*)
  (when (= *sane-references* 1)
    (with-foreign-object (c-version :int)
      (sane-lowlevel:ensure-ok
       (sane-lowlevel::sane_init c-version (null-pointer))
       "Failed to initialise sanelib.")

      (setf *sane-version*
            (sane-interpret-version
             (mem-ref c-version :int)))))
  *sane-version*)

(defun sane-unref ()
  "Decrement SANE reference count and free memory structures relating to the
SANE library if count is zero."
  (decf *sane-references*)
  (if (< 1 *sane-references*)
      (setf *sane-references* 0)
      (sane-lowlevel::sane_exit)))
