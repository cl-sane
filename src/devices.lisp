(in-package :sane)

(defvar *device-list*
  nil
  "Cached list of sane devices")

(defclass device ()
  ((name         :reader   name        :initarg :name)
   (vendor       :reader   vendor      :initarg :vendor)
   (model        :reader   model       :initarg :model)
   (type         :reader   device-type :initarg :type)
   (handle       :accessor handle      :initarg :handle)

   (options      :initform nil)
   (groups       :initform nil)
   (options-hash :initform nil)))

(defmethod print-object ((object device) stream)
  (format stream "#<SANE::DEVICE name=\"~A\"~A>"
          (name object)
          (when (and (slot-boundp object 'handle) (handle object))
            " *")))

(defun device-p (d) (eq (type-of d) 'device))

(defun copy-device (d)
  (unless (device-p d)
    (error "copy-device only works on devices. This is a ~A"
           (type-of d)))
  (let ((ret
         (make-instance 'device
                        :name (name d)
                        :vendor (vendor d)
                        :model (model d)
                        :type (device-type d))))
    (when (slot-boundp d 'handle)
      (setf (handle ret) (handle d)))
    ret))

(defun devices (&optional (local-only t) force-update)
  "Read the list of sane-accessible devices. If LOCAL-ONLY, then only
look for devices local to the machine. If FORCE-UPDATE, search again,
whether or not we've done it already."
  (if (and *device-list* (not force-update))
      *device-list*
      (setf *device-list* (read-devices local-only))))

(defun read-devices (local-only)
  "Read list of sane-accessible devices."
  (with-foreign-object (dlist :pointer)
    (sane-lowlevel::sane_get_devices dlist (if local-only 1 0))
    (let ((plist (mem-ref dlist :pointer)) (pdevice))
      (iterate
        (for i upfrom 0)
        (until (null-pointer-p
                (setf pdevice (mem-aref plist :pointer i))))
        (collect (device-from-ptr pdevice))))))

(defun device-from-ptr (device-ptr)
  "Constructs a device structure from the pointer DEVICE-PTR"
  (make-instance 'device
   :name (foreign-string-to-lisp (mem-aref device-ptr :pointer 0))
   :vendor (foreign-string-to-lisp (mem-aref device-ptr :pointer 1))
   :model (foreign-string-to-lisp (mem-aref device-ptr :pointer 2))
   :type (foreign-string-to-lisp (mem-aref device-ptr :pointer 3))))

(defun calc-device-string (ref)
  "Calculates the correct string to use to open the device with sane_open. nil
gives the first device. An integer n gives the n'th device. A device gives the
device name. A string is left alone."

  ;; Nil => 0
  (unless ref (setf ref 0))

  ;; n => n'th arg
  (when (integerp ref)
    (unless (and (>= ref 0) (> (length (devices)) ref))
      (error "Invalid position in device list: ~A. There are ~A devices."
             ref (length (devices))))
    (setf ref (name (nth ref (devices)))))

  ;; Device => its name
  (when (device-p ref) (setf ref (name ref)))

  ;; Check we've now got a string (either from the above or just falling through
  ;; so that (calc-device-string 'a) doesn't bugger up something else later.
  (unless (stringp ref)
    (error "Invalid type for use as device location: ~A" (type-of ref)))

  ref)

(defmacro with-device (dev-sym dev-ref &body forms)
  "Evaluate FORMS with DEVICE bound to the sane device referred to by
DEV-REF within a progn. This can either be the nil, the name of a
device or a non-negative integer.

If DEV-REF is the name of a device then DEVICE is bound to the
device of that name (if it exists!). If DEV-REF is an integer,
DEVICE is bound to the DEV-REF'th device returned by
sane-devices. If DEV-REF is nil, then DEVICE is bound to the first
element from that list.

DEV-REF (and FORMS!) are only evaluated once."

  (let ((locn-s (gensym))
        (phandle-s (gensym))
        (cname-s (gensym))
        (ret-s (gensym)))
    
    `(let* ((,locn-s (calc-device-string ,dev-ref))
            (,dev-sym
             (copy-device (find-if (lambda (d) (string= ,locn-s (name d)))
                                   (devices))))
            ,ret-s)
      #+sbcl (declare (sb-ext:muffle-conditions sb-ext:code-deletion-note))

      (with-foreign-object (,phandle-s :pointer)
        (with-foreign-string (,cname-s ,locn-s)

          (sane-lowlevel::sane_open ,cname-s ,phandle-s)

          (setf (handle ,dev-sym) (mem-ref ,phandle-s :pointer))

          (unwind-protect
               (setf ,ret-s (progn ,@forms))
            (sane-lowlevel::sane_close (handle ,dev-sym)))))
       
       ;; Evaluate to the result of the internal forms progn.
       ,ret-s)))