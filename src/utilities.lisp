(in-package :sane)

(defmacro ensure-ok (form &rest err_format)
  "Run FORM and ensure it evaluates to :SANE_STATUS_GOOD. If not,
throw an error, with message ERR_FORMAT if defined. A description of
the error is appended in this case."
  
  (let ((r (gensym)))
    `(let ((,r))
       (unless (eq :SANE_STATUS_GOOD (setf ,r ,form))
         (let ((the-sane-ret ,r))
           (error
            ,@(if err_format
                  (append (cons (concatenate 'string (car err_format)
                                             " Error: (~A)")
                                (cdr err_format))
                          '(the-sane-ret))
                  `("Error calling libsane function: ~A" ,r))))))))
